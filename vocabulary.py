#! usr/bin/python
from bs4 import BeautifulSoup
import requests

base_url = "http://www.vocabulary.com/dictionary/"

query_word = raw_input("Word? ")
url = base_url + query_word

def definition(soup):
    relevant_seciton = soup.find("div","main")
    definitions = relevant_seciton.findAll("h3","definition")
    for i in definitions:
        node = i.findAll(text=True)
        #node[0] = "\n \t\t\t\t"
        node[2] = node[2][32:]
        #print node
        print  ''.join(node)
        print "------"

def des_short(soup):
    des = soup.find("p","short")
    if des:
        desc_short = ''.join(des.findAll(text=True))
        print desc_short
        print "------"



def des_long(soup):
    des = soup.find("p","long")
    if des:
        txt_iter = des.stripped_strings
        desc_long = ''
        for txt in txt_iter:
            desc_long = desc_long + txt
            #''.join(i)
        print desc_long
        print "------"


if __name__ == '__main__':
    r = requests.get(url).text
    soup = BeautifulSoup(r)
    definition(soup)
    des_short(soup)
    des_long(soup)
